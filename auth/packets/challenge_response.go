package packets

import "gitlab.com/mark.feller/wowser-logon/auth/codes"

type AuthLogonChallengeResponse struct {
	cmd     byte
	error   byte
	result  byte
	B       []byte // 32 bytes
	gLen    byte
	g       []byte
	NLen    byte
	N       []byte
	s       []byte
	unk3    []byte // Unknown 16 bytes - suspect this is a CRC checksum
	secFlag byte
}

func NewAuthLogonChallengeResponse(N, g, s, B []byte) *AuthLogonChallengeResponse {
	unk3 := make([]byte, 16) //randbytes(16)

	return &AuthLogonChallengeResponse{
		cmd:     codes.CmdAuthLogonChallenge,
		error:   0,
		result:  codes.AuthSuccess,
		B:       B,
		gLen:    byte(len(g)),
		g:       g,
		NLen:    byte(len(N)),
		N:       N,
		s:       s,
		unk3:    unk3,
		secFlag: 0, // security flag is always 0 in vanilla
	}
}

func (p *AuthLogonChallengeResponse) Parse(pkt []byte) error {
	return nil
}

func (p *AuthLogonChallengeResponse) Serialize() (r []byte) {
	r = append(r, p.cmd)
	r = append(r, p.error)
	r = append(r, p.result)
	r = append(r, reverse(p.B)...)
	r = append(r, p.gLen)
	r = append(r, p.g...)
	r = append(r, p.NLen)
	r = append(r, reverse(p.N)...)
	r = append(r, reverse(p.s)...)
	r = append(r, p.unk3...)
	r = append(r, p.secFlag)

	return
}

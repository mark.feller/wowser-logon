package packets

import (
	"errors"
	"fmt"

	"gitlab.com/mark.feller/wowser-logon/auth/codes"
)

type AuthLogonProofResponse struct {
	cmd      byte
	error    byte
	M2       []byte // 20 bytes
	surveyId []byte // 4 bytes
}

func NewAuthLogonProofResponse(M2 []byte) *AuthLogonProofResponse {
	return &AuthLogonProofResponse{
		cmd:      codes.CmdAuthLogonProof,
		error:    0,
		M2:       M2,
		surveyId: []byte{0, 0, 0, 0},
	}
}

func (p *AuthLogonProofResponse) Parse(pkt []byte) (err error) {
	if len(pkt) != 26 {
		err = errors.New("cannot parse logon proof bytes: packet wrong length")
		return
	}

	p.cmd = codes.CmdAuthLogonProof
	p.error = 0
	p.M2 = pkt[2:22]
	p.surveyId = pkt[22:26]

	return
}

func (p *AuthLogonProofResponse) Serialize() (r []byte) {
	r = append(r, p.cmd)
	r = append(r, p.error)
	r = append(r, reverse(p.M2)...)
	r = append(r, p.surveyId...)

	return
}

func (p *AuthLogonProofResponse) String() string {
	str := `
cmd      : %x
error    : %x
M2       : %x
surveyId : %x
`

	return fmt.Sprintf(str, p.cmd, p.error, p.M2, p.surveyId)
}

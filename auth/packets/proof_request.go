package packets

import (
	"errors"
	"fmt"

	"gitlab.com/mark.feller/wowser-logon/auth/codes"
)

type AuthLogonProofRequest struct {
	cmd          byte
	A            []byte
	M1           []byte
	crcHash      []byte
	numberOfKeys byte
	secFlag      byte
}

func NewAuthLogonProofRequest(A, M1 []byte) *AuthLogonProofRequest {
	return &AuthLogonProofRequest{
		cmd:          codes.CmdAuthLogonProof,
		A:            reverse(A),
		M1:           reverse(M1),
		crcHash:      randbytes(20),
		numberOfKeys: 0,
		secFlag:      0,
	}
}

func (p *AuthLogonProofRequest) Parse(pkt []byte) (err error) {
	if len(pkt) != 75 {
		err = errors.New("cannot parse logon proof bytes: packet wrong length")
		return
	}

	p.cmd = pkt[0]
	p.A = make([]byte, 32)
	copy(p.A, reverse(pkt[1:33]))
	// copy(p.A, pkt[1:33])
	p.M1 = make([]byte, 20)
	// copy(p.M1, reverse(pkt[33:53]))
	copy(p.M1, pkt[33:53])
	p.crcHash = pkt[53:73]
	p.numberOfKeys = pkt[73]
	p.secFlag = pkt[74]

	return
}

func (p *AuthLogonProofRequest) Serialize() (r []byte) {
	r = append(r, p.cmd)
	r = append(r, p.A...)
	r = append(r, p.M1...)
	r = append(r, p.crcHash...)
	r = append(r, p.numberOfKeys)
	r = append(r, p.secFlag)

	return nil
}

func (p *AuthLogonProofRequest) String() string {
	str := `
cmd          : %x
A            : %x
M1           : %x
crcHash      : %x
numberOfKeys : %x
secFlag      : %x
`

	return fmt.Sprintf(str, p.cmd, p.A, p.M1, p.crcHash, p.numberOfKeys, p.secFlag)
}

package packets

import (
	"errors"
)

type AuthLogonChallengeRequest struct {
	cmd          byte
	error        byte
	size         byte
	gamename     []byte // 4 bytes
	version1     byte
	version2     byte
	version3     byte
	build        []byte // 2 bytes
	platform     []byte // 4 bytes
	os           []byte // 4 bytes
	country      []byte // 4 bytes
	timezoneBias []byte // 4 bytes
	ip           []byte // 4 bytes
	Ilen         byte
	I            []byte
}

// NewAuthLogonChallengeRequest creates a new AuthLogonChallengeRequest from a packet
func (a *AuthLogonChallengeRequest) Parse(p []byte) (err error) {
	ilen := uint8(p[33])
	if len(p) < int(ilen)+33 {
		err = errors.New("cannot parse logon challenge bytes: packet too short")
		return
	}

	a.cmd = p[0]
	a.error = p[1]
	a.size = p[2]
	a.gamename = p[4:8]
	a.version1 = p[8]
	a.version2 = p[9]
	a.version3 = p[10]
	a.build = p[11:13]
	a.platform = p[13:17]
	a.os = p[17:21]
	a.country = p[21:25]
	a.timezoneBias = p[25:29]
	a.ip = p[29:33]
	a.Ilen = p[33]
	a.I = make([]byte, a.Ilen)
	copy(a.I, p[34:])

	return
}

func (a *AuthLogonChallengeRequest) Serialize() (r []byte) {
	r = append(r, a.cmd)
	r = append(r, a.error)
	r = append(r, a.size)
	r = append(r, a.gamename...)
	r = append(r, a.version1)
	r = append(r, a.version2)
	r = append(r, a.version3)
	r = append(r, a.build...)
	r = append(r, a.platform...)
	r = append(r, a.os...)
	r = append(r, a.country...)
	r = append(r, a.timezoneBias...)
	r = append(r, a.ip...)
	r = append(r, a.Ilen)
	r = append(r, a.I...)

	return
}

package packets

import "errors"

type RealmlistRequest struct {
	cmd byte
}

func (a *RealmlistRequest) Parse(p []byte) (err error) {
	if len(p) != 5 {
		err = errors.New("cannot parse realmlist bytes: packet too short")
		return
	}

	a.cmd = p[0]
	return
}

func (a *RealmlistRequest) Serialize() (r []byte) {
	r = append(r, a.cmd)
	r = append(r, []byte{0x00, 0x00, 0x00, 0x00}...)

	return
}

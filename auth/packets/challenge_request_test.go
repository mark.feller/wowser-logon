package packets

import (
	"encoding/hex"
	"fmt"
	"log"
	"math/big"
	"strings"
	"testing"

	srp "gitlab.com/mark.feller/go-srp"
)

func TestChallengeParser(t *testing.T) {
	p, err := hex.DecodeString("00032200576f5700010c01f316363878006e69570053556e655cfeffffc0a801240454455354")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(p)
	fmt.Println(len(p))

	req := AuthLogonChallengeRequest{}
	req.Parse(p)

	fmt.Println(req.I)
}

func TestProofParser(t *testing.T) {
	p, _ := hex.DecodeString("01acf8433b170bfd842e91bfca083a33fc3f08d162dff086724d6219198aaea23a19e5bbf1aab97316f4aeede3ee270e5c219ee134ffa8e9a7d4b7f6b65177f95a62461d8a550775ab0000")

	req := AuthLogonProofRequest{}
	req.Parse(p)

	A := big.NewInt(0).SetBytes(req.A)
	fmt.Println(req.A)
	fmt.Println(A)

	M1 := big.NewInt(0).SetBytes(req.M1)
	fmt.Println(req.M1)
	fmt.Printf("%x\n", M1)
}

func TestChallengeResp(t *testing.T) {
	p, _ := hex.DecodeString("3d0d99423e31fcc67a6745ec89d70d700344bc76")
	I := []byte("TEST")

	s, v, _ := srp.Verifier(p, 32)
	server, _ := srp.NewServer(I, v, s, 32)

	pkt := NewAuthLogonChallengeResponse(server.Credentials()).Serialize()

	correct := "00000014dc3cff716e71c34791a4c95b51fe4fc58a7c248811f6325c21bee8ab5cc578010720b79b3e2a87823cab8f5ebfbf8eb10108535006298b5badbd5b53e1895e644b896cbc767ab891e5c560defc3d0bb5593710986e6153175257942c86fa6c57592c0000000000000000000000000000000000"

	hpkt := fmt.Sprintf("%x", pkt)

	fmt.Printf("%v\n%v\n", correct, hpkt)

	fmt.Println(strings.Compare(hpkt, correct))

}

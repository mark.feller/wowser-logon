package packets

type Packet interface {
	Parse([]byte) error
	Serialize() []byte
}

package codes

const (
	CmdAuthLogonChallenge     = 0x00
	CmdAuthLogonProof         = 0x01
	CmdAuthReconnectChallenge = 0x02
	CmdAuthReconnectProof     = 0x03
	CmdAuthSurveyData         = 0x04
	CmdRealmList              = 0x10
	CmdXferInitiate           = 0x30
	CmdXferData               = 0x31
	CmdXferAccept             = 0x32
	CmdXferResume             = 0x33
	CmdXferCancel             = 0x34

	AuthSuccess               = 0x00
	AuthFailUnknown0          = 0x01
	AuthFailUnknown1          = 0x02
	AuthFailBanned            = 0x03
	AuthFailUnknownAccount    = 0x04
	AuthFailIncorrectPassword = 0x05
	AuthFailAlreadyOnline     = 0x06
	AuthFailNoTime            = 0x07
	AuthFailDbBusy            = 0x08
	AuthFailVersionInvalid    = 0x09
	AuthFailVersionUpdate     = 0x0A
	AuthFailInvalidServer     = 0x0B
	AuthFailSuspended         = 0x0C
	AuthFailFailNoaccess      = 0x0D
	AuthSuccessSurvey         = 0x0E
	AuthFailParentcontrol     = 0x0F
	AuthFailLockedEnforced    = 0x10
	AuthFailTrialEnded        = 0x11
	AuthFailUseBattlenet      = 0x12
)

package models

import (
	"crypto/sha1"
	"fmt"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Account struct {
	gorm.Model
	Username      string `gorm:"not null;unique"`
	ShaPassHash   string `gorm:"type:varchar(40);not null;"`
	Gmlevel       uint   `gorm:"not null"`
	Sessionkey    string `gorm:"type:longtext"`
	V             string `gorm:"type:longtext"` // SRP-6 Verifier
	S             string `gorm:"type:longtext"` // SRP-6 Salt
	Email         string `gorm:"type:text"`
	Joindate      string `gorm:"type:timestamp;not null"`
	LastIp        string `gorm:"type:varchar(30);not null"`
	FailedLogins  uint   `gorm:"not null"`
	Locked        uint   `gorm:"not null"`
	LastLogin     string `gorm:"type:timestamp;not null"`
	ActiveRealmId uint   `gorm:"not null"`
	Mutetime      uint   `gorm:"type:bigint(40);not null"`
	Locale        uint   `gorm:"type:tinyint(3);not null"`
	RegIp         string `gorm:"type:varchar(30);not null"`
	ClientInfo    string `gorm:"type:varchar(30);not null"`
	Bans          []AccountBanned
}

type AccountBanned struct {
	gorm.Model
	banDate   int    `gorm:"type:bigint(40);not null"`
	unbanDate int    `gorm:"type:bigint(40);not null"`
	bannedBy  string `gorm:"type:varchar(50);not null,"`
	banReason string `gorm:"type:varchar(255);not null,"`
	active    bool   `gorm:"type:tinyint(4);not null"`
}

type IPBanned struct {
	gorm.Model
	IP        string
	banDate   string
	unbanDate string
	bannedBy  string
	banReason string
	active    bool
}

func (a *Account) IsBanned() bool {
	for _, ban := range a.Bans {
		if ban.active {
			return true
		}
	}
	return false
}

func (a *Account) RegularIP(ip string) bool {
	if a.RegIp != "" && a.RegIp != ip {
		return false
	}
	return true
}

type Realmlist struct {
	gorm.Model
	name                 string  `gorm:"type:varchar(32);not null;index:idx_name"`
	address              string  `gorm:"type:varchar(32);not null"`
	port                 int     `gorm:"type:int(11);not null"`
	icon                 uint    `gorm:"type:tinyint(3) unsigned;not null"`
	color                uint    `gorm:"type:tinyint(3) unsigned;not null"`
	timezone             uint    `gorm:"type:tinyint(3) unsigned;not null"`
	allowedSecurityLevel uint    `gorm:"type:tinyint(3) unsigned;not null"`
	population           float64 `gorm:"type:float;not null"`
	realmbuilds          string  `gorm:"type:varchar(64);not null"`
	revision             string  `gorm:"type:varchar(5);not null"`
}

type Uptime struct {
	gorm.Model
	realmid     uint   `gorm:"int(11) unsigned;not null,"`
	starttime   uint   `gorm:"bigint(20) unsigned;not null"`
	startstring string `gorm:"varchar(64);not null"`
	uptime      uint   `gorm:"bigint(20);not null"`
	maxplayers  uint   `gorm:"smallint(5) unsigned;not null"`
}

func CreateAccount(user, pass string) {
	acc := &Account{
		Username:    strings.ToUpper(user),
		ShaPassHash: hashPass(user, pass),
		Joindate:    time.Now().String(),
	}

	db, _ := gorm.Open("sqlite3", "test.db")
	if db.First(&acc) != nil {
		return
	}
	db.Create(acc)
	fmt.Println("created account")

	var account Account
	db.First(&account)
	fmt.Println("account", account)
}

func hashPass(user, pass string) string {
	I := reverseBytes([]byte(strings.ToUpper(user + ":" + pass)))

	for i, j := 0, len(I)-1; i < j; i, j = i+1, j-1 {
		I[i], I[j] = I[j], I[i]
	}

	h := sha1.New()
	h.Write(I)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func reverseBytes(r []byte) []byte {
	for i, j := 0, len(r)-1; i < j; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}

	return r

}

package models

import (
	"bytes"
	"io/ioutil"

	"github.com/BurntSushi/toml"
)

type LogonConfig struct {
	Conf struct {
		Version int `json:"Version"`
	} `json:"Conf"`
	Settings struct {
		LoginDatabaseInfo      string `json:"LoginDatabaseInfo"`
		LogsDir                string `json:"LogsDir"`
		MaxPingTime            int    `json:"MaxPingTime"`
		RealmServerPort        int    `json:"RealmServerPort"`
		BindIP                 string `json:"BindIP"`
		PidFile                string `json:"PidFile"`
		LogLevel               int    `json:"LogLevel"`
		LogTime                int    `json:"LogTime"`
		LogFile                string `json:"LogFile"`
		LogTimestamp           int    `json:"LogTimestamp"`
		LogFileLevel           int    `json:"LogFileLevel"`
		LogColors              string `json:"LogColors"`
		UseProcessors          int    `json:"UseProcessors"`
		ProcessPriority        int    `json:"ProcessPriority"`
		WaitAtStartupError     int    `json:"WaitAtStartupError"`
		RealmsStateUpdateDelay int    `json:"RealmsStateUpdateDelay"`
		WrongPass              struct {
			MaxCount int `json:"MaxCount"`
			BanTime  int `json:"BanTime"`
			BanType  int `json:"BanType"`
		} `json:"WrongPass"`
	} `json:"Settings"`
}

func ReadConfig(file string) (*LogonConfig, error) {
	config := &LogonConfig{}

	f, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	_, err = toml.Decode(bytes.NewBuffer(f).String(), config)
	return config, err
}

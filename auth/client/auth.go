package client

import (
	"errors"
	"fmt"
	"net"

	log "github.com/Sirupsen/logrus"
	srp "gitlab.com/mark.feller/go-srp"
)

type AuthClient struct {
	Logger *log.Logger
	ch     chan []byte
	client *srp.Client
}

func NewAuthClient(user, pass string) *AuthClient {
	c, _ := srp.NewClient([]byte(user), []byte(pass), 32)
	s := &AuthClient{
		client: c,
	}
	return s
}

func (s *AuthClient) WithLogger(l *log.Logger) *AuthClient {
	s.Logger = l
	return s
}

func (c *AuthClient) Open() {

	conn, err := net.Dial("tcp", "localhost:3724")
	if err != nil {
		log.Fatal(err)
	}

	c.ch = make(chan []byte)
	go func() {
		var msg []byte
		for {
			msg = <-c.ch
			log.WithFields(log.Fields{"packet": msg, "length": len(msg)}).Debug("sending message")
			_, err := conn.Write(msg)
			if err != nil {
				log.Println(err)
			}
		}
	}()

	c.sendCredentials()

	b := make([]byte, 512)
	for {
		i, err := conn.Read(b)
		if err != nil {
			log.Println(err)
		}

		if err := dispatch(b[:i]); err != nil {
			c.Logger.Error(err)
			break
		}
	}
}

func (c *AuthClient) sendCredentials() {
	msg := []byte{0, 3, 36, 0, 87, 111, 87, 0, 1, 12, 1, 243, 22, 54, 56, 120, 0, 110, 105, 87, 0, 83, 85, 110, 101, 92, 254, 255, 255, 192, 168, 1, 36, 6, 77, 69, 68, 73, 77, 65}

	c.ch <- msg
}

func dispatch(b []byte) error {
	log.WithFields(log.Fields{"packet": b, "length": len(b)}).Info("dispatcher revieved packet")
	if len(b) == 0 {
		return errors.New("invalid command: empty packet")
	}
	// if fn, ok := handlers[b[0]]; ok {
	//	return fn(b)
	// }
	err := fmt.Sprintf("invalid command: %v", b)
	return errors.New(err)
}

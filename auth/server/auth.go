package server

import (
	"errors"
	"fmt"
	"net"

	log "github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	srp "gitlab.com/mark.feller/go-srp"
	"gitlab.com/mark.feller/wowser-logon/auth/codes"
	"gitlab.com/mark.feller/wowser-logon/auth/models"
	"gitlab.com/mark.feller/wowser-logon/auth/packets"
)

type AuthHandler func([]byte) error

var handlers map[byte]AuthHandler

type AuthServer struct {
	Logger *log.Logger
	db     *gorm.DB
	ch     chan packets.Packet // channel is used for sending replies to connection
	server *srp.Server
}

func NewAuthServer() *AuthServer {
	s := &AuthServer{}
	s.initHandlers()
	return s
}

func (s *AuthServer) initHandlers() {
	handlers = make(map[byte]AuthHandler)

	handlers[codes.CmdAuthLogonChallenge] = s.handleAuthChallenge
	handlers[codes.CmdAuthLogonProof] = s.handleLogonProof
	handlers[codes.CmdRealmList] = s.handleRealmlist
}

func (s *AuthServer) WithDB(conn string) *AuthServer {
	db, _ := gorm.Open("sqlite3", conn)
	s.db = db
	return s
}

func (s *AuthServer) WithLogger(l *log.Logger) *AuthServer {
	log.SetLevel(log.InfoLevel)
	s.Logger = l
	return s
}

func (s *AuthServer) HandleConnection(conn net.Conn) {
	defer conn.Close()

	l := s.Logger.WithField("addr", conn.RemoteAddr().String())
	l.Info("accepting connection")

	// Check for IP Ban
	if s.isIPBanned(conn.RemoteAddr().String()) {
		return
	}

	s.ch = make(chan packets.Packet)
	go func() {
		for {
			pkt := <-s.ch
			msg := pkt.Serialize()

			_, err := conn.Write(msg)
			if err != nil {
				log.Println(err)
			}
			l.WithFields(log.Fields{"packet": fmt.Sprintf("%x", msg), "length": len(msg)}).Debug("sent message")
		}
	}()

	b := make([]byte, 512)
	for {
		i, err := conn.Read(b)
		if err != nil {
			log.Println(err)
		}
		l.WithField("packet", b[:i]).Debug("recieved packet")
		if err := dispatch(b[:i]); err != nil {
			s.Logger.Error(err)
			break
		}
	}

	l.Info("done handling connection")
	return
}

func dispatch(b []byte) error {
	if len(b) == 0 {
		return errors.New("invalid command: empty packet")
	} else if fn, ok := handlers[b[0]]; ok {
		return fn(b)
	}

	err := fmt.Sprintf("invalid command: %x", b[0])
	return errors.New(err)
}

// TODO: refactor this
func (s *AuthServer) sendFailedLogon(code byte) {
	s.Logger.WithField("err_code", code).Info("failed logon attempt")
	//s.ch <- []byte{codes.CmdAuthLogonChallenge, 0x00, code}
}

func (s *AuthServer) isIPBanned(ip string) bool {
	ipban := models.IPBanned{IP: ip}
	s.db.Where(&ipban).First(&ipban)
	return false
}

func (s *AuthServer) handleLogonProof(b []byte) (err error) {
	log.WithFields(log.Fields{"packet": fmt.Sprintf("%x", b), "length": len(b)}).Debug("handling logon proof")

	pr := &packets.AuthLogonProofRequest{}
	err = pr.Parse(b)
	if err != nil {
		return err
	}

	// Valid version

	// SRP-6 verfiy client M1
	m2, err := s.server.ClientOK(pr.A, pr.M1)
	if err != nil {
		return err
	}

	s.ch <- packets.NewAuthLogonProofResponse(m2)
	return
}

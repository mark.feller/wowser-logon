package server

import (
	log "github.com/Sirupsen/logrus"
	"gitlab.com/mark.feller/wowser-logon/auth/packets"
)

func (s *AuthServer) handleRealmlist(p []byte) (err error) {
	// log.WithFields(log.Fields{"packet": p, "length": len(p)}).Debug("handling realmlist")

	rl := &packets.RealmlistRequest{}
	err = rl.Parse(p)
	if err != nil {
		return
	}

	return nil
}

func (s *AuthServer) loadRealmlist() {
	log.Debug("handling realmlist")

	// var pkt []byte
}

type RealmlistResponse struct {
	size      int
	numRealms byte
	realms    []Realm
	unknown   int // always 2
}

type Realm struct {
}

package server

import (
	"bytes"
	"encoding/hex"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/mark.feller/go-srp"
	"gitlab.com/mark.feller/wowser-logon/auth/models"
	"gitlab.com/mark.feller/wowser-logon/auth/packets"
)

func (s *AuthServer) handleAuthChallenge(p []byte) (err error) {
	log.WithFields(log.Fields{"packet": p, "length": len(p)}).Debug("handling auth challenge")

	ch := &packets.AuthLogonChallengeRequest{}
	err = ch.Parse(p)
	if err != nil {
		return
	}

	user := bytes.NewBuffer(ch.I).String()
	s.Logger.WithField("account", user).Info("logon attempt")

	// Get Account info from DB
	acc := &models.Account{Username: user}
	s.db.Where(acc).First(acc)

	// // check for account existence
	// if user.ID == 0 {
	//	return s.sendFailedLogon(conn, wowFailIncorrectPassword)
	// }

	// // check for account ban status
	// if acc.IsBanned() {
	//	s.sendFailedLogon(wowFailBanned)
	//	return
	// }

	// // check for IP address
	// if !acc.RegularIP(conn.RemoteAddr().String()) {
	//	return s.sendFailedLogon(conn, wowFailSuspended)
	// }

	s.sendSuccessfulLogon(acc, ch)
	return
}

func (s *AuthServer) sendSuccessfulLogon(acc *models.Account, ch *packets.AuthLogonChallengeRequest) {
	s.Logger.WithFields(log.Fields{
		"account": acc.Username,
	}).Info("authentication values")

	// check salt and verifier are properly assigned
	s.verifySalt(acc, ch)

	v, _ := hex.DecodeString(acc.V)
	salt, _ := hex.DecodeString(acc.S)
	server, _ := srp.NewServer(ch.I, v, salt, 32)
	s.server = server

	s.ch <- packets.NewAuthLogonChallengeResponse(s.server.Credentials())
}

func (s *AuthServer) verifySalt(acc *models.Account, ch *packets.AuthLogonChallengeRequest) {
	if acc.S == "" || acc.V == "" {
		log.WithField("account", acc.Username).Info("creating new salt and verifier")
		p, _ := hex.DecodeString(acc.ShaPassHash)
		salt, v, _ := srp.Verifier(p, 32)
		acc.S = hex.EncodeToString(salt)
		acc.V = hex.EncodeToString(v)
		s.db.Save(acc)
	}
}

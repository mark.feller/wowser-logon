package run

import (
	"strings"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/mark.feller/wowser-logon/auth/client"
)

type RunClientCommand struct {
}

func (c *RunClientCommand) Run(args []string) int {
	log.SetLevel(log.DebugLevel)

	// scanner := bufio.NewScanner(os.Stdin)

	// fmt.Print("username: ")
	// scanner.Scan()
	// user := scanner.Text()

	// fmt.Print("password: ")
	// scanner.Scan()
	// pass := scanner.Text()

	// if err := scanner.Err(); err != nil {
	//	fmt.Fprintln(os.Stderr, "reading standard input:", err)
	// }

	// fmt.Printf("user '%s'\n", user)
	// fmt.Printf("pass '%s'\n", pass)

	// authClient := client.NewAuthClient(user, pass).WithLogger(log.New())
	authClient := client.NewAuthClient("medima", "ezguess").WithLogger(log.New())

	authClient.Open()

	return 0
}

func (c *RunClientCommand) Synopsis() string {
	return ""
}

func (c *RunClientCommand) Help() string {
	helpText := `

`
	return strings.TrimSpace(helpText)
}

package run

import (
	"flag"
	"net"
	"strings"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/mark.feller/wowser-logon/auth/models"
	"gitlab.com/mark.feller/wowser-logon/auth/server"
)

type RunServerCommand struct {
	Config string
}

func (c *RunServerCommand) Run(args []string) int {
	log.SetLevel(log.DebugLevel)
	authServer := server.NewAuthServer().WithLogger(log.New()).WithDB("test.db")

	cmdFlags := flag.NewFlagSet("run", flag.ContinueOnError)
	cmdFlags.StringVar(&c.Config, "c", "", "target Directory")

	if err := cmdFlags.Parse(args); err != nil {
		return 1
	}

	if c.Config != "" {
		_, err := models.ReadConfig(c.Config)
		if err != nil {
			log.WithField("err", err).Error("could not read config file")
			return 1
		}
	}

	models.CreateAccount("test2", "test2")

	// Listen on socket deafult port 3724
	ln, err := net.Listen("tcp", ":3724")
	if err != nil {
		log.WithField("err", err).Error("could not listen on port")
		return 1
	}
	log.Info("logon daemon started")
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go authServer.HandleConnection(conn)
	}

	log.Info("logon daemon shutting down")
	return 0
}

func (c *RunServerCommand) Synopsis() string {
	return ""
}

func (c *RunServerCommand) Help() string {
	helpText := `
Usage: wowser run [<options>]
	-v, --version            print version and exist
	-c config_file           use config_file as configuration file
`
	return strings.TrimSpace(helpText)
}

package command

import (
	"strings"

	"github.com/mitchellh/cli"
	"gitlab.com/mark.feller/wowser-logon/command/run"
)

type RunCommand struct {
	Meta
}

func (c *RunCommand) Run(args []string) int {
	runc := cli.NewCLI("sub command run", "")
	runc.Args = args

	runc.Commands = map[string]cli.CommandFactory{
		"server": func() (cli.Command, error) {
			return &run.RunServerCommand{}, nil
		},
		"client": func() (cli.Command, error) {
			return &run.RunClientCommand{}, nil
		},
	}

	if exitStatus, err := runc.Run(); err != nil {
		return exitStatus
	} else {
		return exitStatus
	}

	return 0
}

func (c *RunCommand) Synopsis() string {
	return ""
}

func (c *RunCommand) Help() string {
	helpText := `
Usage: wowser run [<command>] [<options>]
	-v, --version            print version and exist

	server                   start logon server
	client                   run logon client for testing
`
	return strings.TrimSpace(helpText)
}
